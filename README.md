# Hasura github auto deploy

## 1. Init hasura project

```bash
hasura init <project-name> --endpoint <endpoint> --admin-secret <admin-secret>
```

## 2. Create migration files

```bash
hasura migrate create "init" --from-server --database-name <database-name> --schema <schema>
```

## 3. Connect to github

![](./assets/readme/sign_in_github.png)

- Click on the button to sign in with your github account

## 4. Integrate repo with the hasura project

![](./assets/readme/connect_repo.png)

- Connect your github account and select the necessary repo
- Select the needed branch from which migrations are to be picked up from
- Select the deployment mode to be auto
